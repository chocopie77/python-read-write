from collections import Counter
text = open('input.txt', 'r').read()
print(text)
f = open('output.txt', 'w')
counts = Counter(text) 
for character, count in counts.most_common():
    f.write(character)
    f.write('-')
    f.write(str(count))
    f.write('\n')
    print(character, '-', count)
f.close()

